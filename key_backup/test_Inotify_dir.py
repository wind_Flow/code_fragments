import logging
import os
import pyinotify
import functools
import sys
import hashlib

logging.basicConfig(filename="Logger.log", level=logging.INFO)

class Counter(object):
    def __init__(self):
        self.changes_amount = 0
    def newchange(self):
        self.changes_amount += 1

def on_loop(notifier, counter):
    cwd = os.getcwd()  # Get the current working directory (cwd)
    cwd = cwd + "/keys"
    files = os.listdir(cwd)

    for filename in files:
        counter.newchange()

        with open("keys/" + filename, "r") as f:
            fileText = f.read()

        with open("../logs_to_send/" + filename, "w") as f:
            f.write(fileText)

    return False


directory_to_watch = "keys/"
wm = pyinotify.WatchManager()
notifier = pyinotify.Notifier(wm)
logging.info(wm.add_watch(directory_to_watch, pyinotify.IN_MODIFY))    #pycharm уверяет, что такого атрибута нет, он ошибается

on_loop_func = functools.partial(on_loop, counter=Counter())

logging.info(notifier.loop(callback=on_loop_func))